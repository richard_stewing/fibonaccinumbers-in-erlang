-module (fibnacci).
-export([calculate/1]).

-spec(calculate(integer() )->list()).
calculate(N)->
	Result = calc(0,1,1, N),
	Result.

-spec(calc(number(), number(), number(), number() )-> list()).
calc(First, Second, Count, Index)->
	calc(First, Second, Count, Index, [] ).


-spec(calc(number(), number(), number(), number(), list() )-> list()).
calc(First, Second, Count, Index, State)->
	if 
		Count < Index ->
			Result = First + Second,
			NewState = lists:append(State, [Result]),
			calc(Second, Result, Count+1, Index, NewState);
		Index == 0->
			NewState = lists:append(State , [0]),
			NewState;
		Count == Index->
			lists:append([0,1], State)
	end.
